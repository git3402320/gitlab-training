class Car:
    def __init__(self, make, model, year):
        self.make = make
        self.model = model
        self.year = year

    def get_name(self):
        long_name = f"{self.year} {self.make} {self.model}"
        return long_name.title()

my_car = Car('Hyundai', 'i40', 2012)    
print(my_car.get_name())